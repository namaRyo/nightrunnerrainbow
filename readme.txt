Night Runner Rainbow

FUSE設定
	0xFF
	0xDA
	0xE2

BODLEVEL = DISABLED
RSTDISBL = [ ]
DWEN = [ ]
SPIEN = [X]
WDTON = [ ]
EESAVE = [ ]
BOOTSZ = 1024W_3C00
BOOTRST = [X]
CKDIV8 = [ ]
CKOUT = [ ]
SUT_CKSEL = INTRCOSC_8MHZ_6CK_14CK_65MS

EXTENDED = 0xFF (valid)
HIGH = 0xDA (valid)
LOW = 0xE2 (valid)


Arduino IDEでのボード設定
	board:		Arduino LilyPad
	processor:	ATmega328

ブートローダー
	atmegaBoot_168_atmega328_pro8MHz.hex

