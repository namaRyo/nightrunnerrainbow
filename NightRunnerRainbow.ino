/**
 * Night Runner Rainbow
 *
 * Board = LilyPad Arduino
 * Processor = ATmega328
 */

#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>

#define PIN            12
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(13, PIN);

int8_t r, g, b;
int mode;
boolean sw_flag;
long intTime=0;
long debounce=300;
int modeAddr = 0;

void sw_int()
{
  if(millis()-intTime > debounce){
    mode++;
    if(mode>=13) mode=0;
    sw_flag = true;
    intTime = millis();
  }
}

void setup()
{
  pixels.begin();
  pixels.setBrightness(50);
  randomSeed(analogRead(A0));
  mode = EEPROM.read(modeAddr);
  sw_flag = true;
  pinMode(2, INPUT_PULLUP);
  attachInterrupt(0, sw_int, FALLING);
}

const uint8_t eyepat[20][13] = {
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {   0,  0,  0,  0,  0,  0,  5,  0,  0,  0,  0,  0,  0},
  {   0,  0,  0,  0,  0,  5, 15,  5,  0,  0,  0,  0,  0},
  {   0,  0,  0,  0,  5, 15, 31, 15,  5,  0,  0,  0,  0},
  {   0,  0,  0,  5, 15, 31, 63, 31, 15,  5,  0,  0,  0},
  {   0,  0,  5, 15, 31, 63,127, 63, 31, 15,  5,  0,  0},
  {   0,  5, 15, 31, 63,127,255,127, 63, 31, 15,  5,  0},
  {   5, 15, 31, 63,127,255,255,255,127, 63, 31, 15,  5},
  {  15, 31, 63,127,255,255,255,255,255,127, 63, 31, 15},
  {  31, 63,127,255,255,255,255,255,255,255,127, 63, 31},
  {  63,127,255,255,255,255,255,255,255,255,255,127, 63},
  {  31, 63,127,255,255,255,255,255,255,255,127, 63, 31},
  {  15, 31, 63,127,255,255,255,255,255,127, 63, 31, 15},
  {   5, 15, 31, 63,127,255,255,255,127, 63, 31, 15,  5},
  {   0,  5, 15, 31, 63,127,255,127, 63, 31, 15,  5,  0},
  {   0,  0,  5, 15, 31, 63,127, 63, 31, 15,  5,  0,  0},
  {   0,  0,  0,  5, 15, 31, 63, 31, 15,  5,  0,  0,  0},
  {   0,  0,  0,  0,  5, 15, 31, 15,  5,  0,  0,  0,  0},
  {   0,  0,  0,  0,  0,  5, 15,  5,  0,  0,  0,  0,  0},
  {   0,  0,  0,  0,  0,  0,  5,  0,  0,  0,  0,  0,  0},
};


const uint8_t pat[44][13] = {
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  { 127,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  { 255,127,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  { 255,255,127,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  { 255,255,255,127,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  { 127,255,255,255,127,  0,  0,  0,  0,  0,  0,  0,  0},
  {  63,127,255,255,255,127,  0,  0,  0,  0,  0,  0,  0},
  {  31, 63,127,255,255,255,127,  0,  0,  0,  0,  0,  0},
  {  15, 31, 63,127,255,255,255,127,  0,  0,  0,  0,  0},
  {   5, 15, 31, 63,127,255,255,255,127,  0,  0,  0,  0},
  {   0,  5, 15, 31, 63,127,255,255,255,127,  0,  0,  0},
  {   0,  0,  5, 15, 31, 63,127,255,255,255,127,  0,  0},
  {   0,  0,  0,  5, 15, 31, 63,127,255,255,255,127,  0},
  {   0,  0,  0,  0,  5, 15, 31, 63,127,255,255,255,127},
  {   0,  0,  0,  0,  0,  5, 15, 31, 63,127,255,255,255},
  {   0,  0,  0,  0,  0,  0,  5, 15, 31, 63,127,255,255},
  {   0,  0,  0,  0,  0,  0,  0,  5, 15, 31, 63,127,255},
  {   0,  0,  0,  0,  0,  0,  0,  0,  5, 15, 31, 63,127},
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,  5, 15, 31, 63},
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  5, 15, 31},
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  5, 15},
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  5},
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,127},
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,127,255},
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,127,255,255},
  {   0,  0,  0,  0,  0,  0,  0,  0,  0,127,255,255,255},
  {   0,  0,  0,  0,  0,  0,  0,  0,127,255,255,255,127},
  {   0,  0,  0,  0,  0,  0,  0,127,255,255,255,127, 63},
  {   0,  0,  0,  0,  0,  0,127,255,255,255,127, 63, 31},
  {   0,  0,  0,  0,  0,127,255,255,255,127, 63, 31, 15},
  {   0,  0,  0,  0,127,255,255,255,127, 63, 31, 15,  5},
  {   0,  0,  0,127,255,255,255,127, 63, 31, 15,  5,  0},
  {   0,  0,127,255,255,255,127, 63, 31, 15,  5,  0,  0},
  {   0,127,255,255,255,127, 63, 31, 15,  5,  0,  0,  0},
  { 127,255,255,255,127, 63, 31, 15,  5,  0,  0,  0,  0},
  { 255,255,255,127, 63, 31, 15,  5,  0,  0,  0,  0,  0},
  { 255,255,127, 63, 31, 15,  5,  0,  0,  0,  0,  0,  0},
  { 255,127, 63, 31, 15,  5,  0,  0,  0,  0,  0,  0,  0},
  { 127, 63, 31, 15,  5,  0,  0,  0,  0,  0,  0,  0,  0},
  {  63, 31, 15,  5,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {  31, 15,  5,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {  15,  5,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {   5,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
};

void loop()
{
  if(sw_flag){
    sw_flag = false;
    DispMode(mode, 1000);
    EEPROM.write(modeAddr, mode);
  }
  if(sw_flag) return;
  int mode2 = mode;
  if(mode2==12){
    mode2=random(12);
  }

  switch(mode2){
  case 0:
    rainbowScan(30);
    break;
  case 1:
    randomScan(20,45);
    break;
  case 2:
    crossScan(20, 40);
    break;
  case 3:
    chase(10, 25, 33, 40);
    break;
  case 4:
    randomLight(100, 30);
    break;
  case 5:
    sideFlash(20, 20, 33);
    break;
  case 6:
    colorWipeRGB(3, 50);
    break;
  case 7:
    rainbow(2, 20);
    break;
  case 8:
    rainbowCycle(5, 20);
    break;
  case 9:
    fumikiri(20, 400);
    break;
  case 10:
    redEye(20, 40);
    break;
  case 11:
    rainbowEyeScan(30);
    break;
  default:
    mode = 0;
    sw_flag = true;
    break;
  }
}

void chase(int cnt, int dlyR, int dlyG, int dlyB)
{
  int cr, cg, cb;
//  for(int lp=0;lp<cnt;lp++){
  for(int lp=0;lp<1;lp++){
    cr = cg = cb = 0;
//    uint32_t dly = dlyR * dlyG * dlyB;
    uint32_t dly = 1000 * cnt;
    for(uint32_t i=0;i<dly;i++){
      boolean flag = false;
      if((i%dlyR)==0){
        cr++;
        if(cr>=44) cr=0;
        flag = true;
      }
      if((i%dlyG)==0){
        cg++;
        if(cg>=44) cg=0;
        flag = true;
      }
      if((i%dlyB)==0){
        cb++;
        if(cb>=44) cb=0;
        flag = true;
      }
      
      if(flag==true){
        for(uint8_t c=0;c<13;c++){
          pixels.setPixelColor(c, pat[cr][c], pat[cg][c], pat[cb][c]);
        }
        pixels.show();
      }
      
      delay(1);
      if(sw_flag) return;
    }
  }
}

void Break(uint16_t dly)
{
  for(uint8_t c=0;c<13;c++)
  {
    pixels.setPixelColor(c, 0, 0, 0);
  }
  pixels.show();
  for(int i=0;i<dly;i++){
    delay(1);
    if(sw_flag) return;
  }
}

void colorWipeRGB(int cnt, int dly)
{
  clear();
  for(int i=0;i<cnt;i++){
    colorWipe(pixels.Color(255, 0, 0), dly); // Red
    if(sw_flag) return;
    colorWipe(pixels.Color(0, 255, 0), dly); // Green
    if(sw_flag) return;
    colorWipe(pixels.Color(0, 0, 255), dly); // Blue
    if(sw_flag) return;
  }
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<pixels.numPixels(); i++) {
      pixels.setPixelColor(i, c);
      pixels.show();
      delay(wait);
      if(sw_flag) return;
  }
}

void rainbow(int cnt, uint8_t wait) {
  uint16_t i, j;

  for(int lp=0;lp<cnt;lp++){
    for(j=0; j<256; j++) {
      for(i=0; i<pixels.numPixels(); i++) {
        pixels.setPixelColor(i, Wheel((i*3+j) & 255));
      }
      pixels.show();
      delay(wait);
      if(sw_flag) return;
    }
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos)
{
  if(WheelPos < 85) {
   return pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   return pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170;
   return pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(int cnt, uint8_t wait)
{
  uint16_t i, j;
  
  for(int lp=0;lp<cnt;lp++){
    for(j=0; j<256; j++) {
      for(i=0; i< pixels.numPixels(); i++) {
        pixels.setPixelColor(i, Wheel(((i * 256 / pixels.numPixels()) + j) & 255));
      }
      pixels.show();
      delay(wait);
      if(sw_flag) return;
    }
  }
}


void randomLight(int cnt, int dly)
{
  for(int i=0;i<cnt;i++){
    for(uint8_t c=0;c<13;c++){
      pixels.setPixelColor(c, random(256), random(256), random(256));
    }
    pixels.show();
    delay(dly);
    if(sw_flag) return;
  }
}

void rainbowScan(int dly)
{
  int i;
  g = b = 0;
  r = 8;
  scan(r,g,b,dly);
  for(i=0;i<8;i++){
    g++;
    scan(r,g,b,dly);
    if(sw_flag) return;
  }
  scan(r,g,b,dly);
  for(i=0;i<8;i++){
    r--;
    scan(r,g,b,dly);
    if(sw_flag) return;
  }
  scan(r,g,b,dly);
  for(i=0;i<8;i++){
    b++;
    scan(r,g,b,dly);
    if(sw_flag) return;
  }
  scan(r,g,b,dly);
  for(i=0;i<8;i++){
    g--;
    scan(r,g,b,dly);
    if(sw_flag) return;
  }
  scan(r,g,b,dly);
  for(i=0;i<8;i++){
    r++;
    scan(r,g,b,dly);
    if(sw_flag) return;
  }
  scan(r,g,b,dly);
  for(i=0;i<8;i++){
    b--;
    scan(r,g,b,dly);
    if(sw_flag) return;
  }
}

void rainbowEyeScan(int dly)
{
  int i;
  g = b = 0;
  r = 8;
  eyescan(r,g,b,dly);
  for(i=0;i<8;i++){
    g++;
    eyescan(r,g,b,dly);
    if(sw_flag) return;
  }
  eyescan(r,g,b,dly);
  for(i=0;i<8;i++){
    r--;
    eyescan(r,g,b,dly);
    if(sw_flag) return;
  }
  eyescan(r,g,b,dly);
  for(i=0;i<8;i++){
    b++;
    eyescan(r,g,b,dly);
    if(sw_flag) return;
  }
  eyescan(r,g,b,dly);
  for(i=0;i<8;i++){
    g--;
    eyescan(r,g,b,dly);
    if(sw_flag) return;
  }
  eyescan(r,g,b,dly);
  for(i=0;i<8;i++){
    r++;
    eyescan(r,g,b,dly);
    if(sw_flag) return;
  }
  eyescan(r,g,b,dly);
  for(i=0;i<8;i++){
    b--;
    eyescan(r,g,b,dly);
    if(sw_flag) return;
  }
}


void redEye(int cnt, int dly)
{
  for(int lp=0;lp<cnt;lp++){
    eyescan(8,0,0,dly);
    if(sw_flag) return;
  }
}

void scan(int8_t r, int8_t g, int8_t b, int dly)
{
  if(r>8)r=8;
  if(g>8)g=8;
  if(b>8)b=8;
  if(r<0)r=0;
  if(g<0)g=0;
  if(b<0)b=0;

  for(uint8_t l=0;l<1;l++)
  {
    for(uint8_t i=0;i<44;i++)
    {
      for(uint8_t c=0;c<13;c++)
      {
        pixels.setPixelColor(c, pat[i][c]>>(8-r), pat[i][c]>>(8-g), pat[i][c]>>(8-b));
      }
      pixels.show();
      delay(dly);
      if(sw_flag) return;
    }
  }
}

void randomScan(int cnt, int dly)
{
  for(int lp=0;lp<cnt;lp++){
    uint8_t r = random(6);
    uint8_t g = random(6);
    uint8_t b = random(6);
    for(uint8_t i=0;i<44;i++){
      for(uint8_t c=0;c<13;c++){
        pixels.setPixelColor(c, pat[i][c]>>r, pat[i][c]>>g, pat[i][c]>>b);
      }
      pixels.show();
      delay(dly);
      if(sw_flag) return;
    }
  }
}

void eyescan(int8_t r, int8_t g, int8_t b, int dly)
{
  if(r>8)r=8;
  if(g>8)g=8;
  if(b>8)b=8;
  if(r<0)r=0;
  if(g<0)g=0;
  if(b<0)b=0;

  for(uint8_t l=0;l<1;l++)
  {
    for(uint8_t i=0;i<20;i++)
    {
      for(uint8_t c=0;c<13;c++)
      {
        pixels.setPixelColor(c, eyepat[i][c]>>(8-r), eyepat[i][c]>>(8-g), eyepat[i][c]>>(8-b));
      }
      pixels.show();
      delay(dly);
      if(sw_flag) return;
    }
  }
}

void crossScan(int cnt, int dly)
{
  for(int lp=0;lp<cnt;lp++){
    for(uint8_t i=0;i<44;i++){
      uint8_t j=i+22;
      if(j>=44) j-=44;
      for(uint8_t c=0;c<13;c++){
        pixels.setPixelColor(c, pat[i][c], 0, pat[j][c]);
      }
      pixels.show();
      delay(dly);
      if(sw_flag) return;
    }
  }
}

void sideFlash(int cnt, int dlyR, int dlyB)
{
  clear();
  uint16_t dly = 100 * cnt * 3;
  dlyR *= 2;
  dlyB *= 2;
  int cntR = 0;
  int cntB = 0;
  for(uint16_t i=0;i<dly;i++, cntR++, cntB++){
    boolean flag = false;
    if((cntR>=0)&&(cntR<dlyR*2)){
      if(cntR%3==0){
        setPixels(0,5,255,0,0);
      }else{
        setPixels(0,5,0,0,0);
      }
      flag = true;
    }
    if(cntR==dlyR*2){
      setPixels(0,5,0,0,0);
      flag = true;
    }
    if(cntR==dlyR*3){
      cntR = 0;
    }

    if((cntB>=0)&&(cntB<dlyB*2)){
      if(cntB%3==0){
        setPixels(7,12,0,0,255);
      }else{
        setPixels(7,12,0,0,0);
      }
      flag = true;
    }
    if(cntB==dlyB*2){
      setPixels(7,12,0,0,0);
      flag = true;
    }
    if(cntB==dlyB*3){
      cntB = 0;
    }

    if(flag==true){
      pixels.show();
    }
    delay(3);
    if(sw_flag) return;
  }
}

void setPixels(int start, int end, int r, int g, int b)
{
  for(uint8_t c=start;c<=end;c++){
    pixels.setPixelColor(c, r, g, b);
  }
}


void clear()
{
  for(uint8_t c=0;c<=12;c++)
    pixels.setPixelColor(c, 0, 0, 0);
}

void DispMode(int mode, uint16_t dly)
{
  for(uint8_t c=0;c<13;c++){
    pixels.setPixelColor(c, 0, 0, 0);
  }
  pixels.setPixelColor(mode, 255, 0, 0);
  pixels.show();
  for(int i=0;i<dly;i++){
    delay(1);
    if(sw_flag) return;
  }
}

void LedCheck(uint16_t dly)
{
  for(uint8_t i=0;i<13;i++){
    for(uint8_t c=0;c<=i;c++){
      pixels.setPixelColor(c, 255, 0, 0);
    }
    pixels.show();
    delay(dly);
    if(sw_flag) return;

    for(uint8_t c=0;c<=i;c++){
      pixels.setPixelColor(c, 0, 255, 0);
    }
    pixels.show();
    delay(dly);
    if(sw_flag) return;

    for(uint8_t c=0;c<=i;c++){
      pixels.setPixelColor(c, 0, 0, 255);
    }
    pixels.show();
    delay(dly);
    if(sw_flag) return;
  }
}

void fumikiri(int cnt, int dly)
{
  for(int lp=0;lp<cnt;lp++){
    for(int c=0;c<13;c++){
      if(c%2==0)  pixels.setPixelColor(c, Wheel(((lp*8+c+1)*5)&255));
      else        pixels.setPixelColor(c,0,0,0);
    }
    pixels.show();
    delay(dly);
    if(sw_flag) return;
      
    for(int c=0;c<13;c++){
      if(c%2==0)  pixels.setPixelColor(c, 0, 0, 0);
      else        pixels.setPixelColor(c,Wheel(((lp*8+c+5)*5)&255));
    }
    pixels.show();
    delay(dly);
    if(sw_flag) return;
  }
}

